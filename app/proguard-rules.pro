-dontobfuscate

-dontwarn com.github.axet.wget.**

-keep class go.** {*;}
-keep class libtorrent.** {*;}

-keep public class com.google.android.exoplayer2.** {*;}
