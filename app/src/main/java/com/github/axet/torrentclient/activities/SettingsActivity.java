package com.github.axet.torrentclient.activities;


import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.MenuItem;

import com.github.axet.androidlibrary.activities.AppCompatSettingsThemeActivity;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.preferences.StoragePathPreferenceCompat;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.services.TorrentService;

import java.io.File;

public class SettingsActivity extends AppCompatSettingsThemeActivity {

    public static final int RESULT_STORAGE = 1;

    @Override
    public int getAppTheme() {
        return TorrentApplication.getTheme(this, R.style.AppThemeLight, R.style.AppThemeDark);
    }

    @Override
    public String getAppThemeKey() {
        return TorrentApplication.PREFERENCE_THEME;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        showSettingsFragment(new GeneralPreferenceFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        super.onSharedPreferenceChanged(sharedPreferences, key);
        if (key.equals(TorrentApplication.PREFERENCE_STORAGE)) {
            String path = sharedPreferences.getString(TorrentApplication.PREFERENCE_STORAGE, "");

            if (path.startsWith(ContentResolver.SCHEME_FILE)) {
                File f = Storage.getFile(Uri.parse(path));
                if (!f.canWrite()) {
                    AlertDialog.Builder b = new AlertDialog.Builder(this);
                    b.setTitle(R.string.storage_path);
                    b.setMessage(R.string.filedialog_readonly);
                    AlertDialog d = b.create();
                    d.show();
                    SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    String old = shared.getString(TorrentApplication.PREFERENCE_STORAGE, ""); // old
                    editor.putString(TorrentApplication.PREFERENCE_STORAGE, old);
                    editor.commit();
                    return; // ignore migrate
                }
            }

            Storage storage = TorrentApplication.from(this).storage;
            storage.migrateLocalStorageDialog(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
        MainActivity.startActivity(this);
    }

    public static class GeneralPreferenceFragment extends PreferenceFragmentCompat {
        public GeneralPreferenceFragment() {
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            OptimizationPreferenceCompat optimization = (OptimizationPreferenceCompat) findPreference(TorrentApplication.PREFERENCE_OPTIMIZATION);
            optimization.enable(TorrentService.class);

            bindPreferenceSummaryToValue(findPreference(TorrentApplication.PREFERENCE_SCREENLOCK));
            bindPreferenceSummaryToValue(findPreference(TorrentApplication.PREFERENCE_THEME));
            bindPreferenceSummaryToValue(findPreference(TorrentApplication.PREFERENCE_ANNOUNCE));

            Storage storage = (TorrentApplication.from(getContext())).storage;
            StoragePathPreferenceCompat s = (StoragePathPreferenceCompat) findPreference(TorrentApplication.PREFERENCE_STORAGE);
            s.setStorage(storage);
            s.setPermissionsDialog(this, Storage.PERMISSIONS_RW, RESULT_STORAGE);
            if (Build.VERSION.SDK_INT >= 21)
                s.setStorageAccessFramework(this, RESULT_STORAGE);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                getActivity().onBackPressed();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onResume() {
            super.onResume();
            OptimizationPreferenceCompat optimization = (OptimizationPreferenceCompat) findPreference(TorrentApplication.PREFERENCE_OPTIMIZATION);
            optimization.onResume();
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

            StoragePathPreferenceCompat s = (StoragePathPreferenceCompat) findPreference(TorrentApplication.PREFERENCE_STORAGE);

            switch (requestCode) {
                case RESULT_STORAGE:
                    s.onRequestPermissionsResult(permissions, grantResults);
                    break;
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            StoragePathPreferenceCompat s = (StoragePathPreferenceCompat) findPreference(TorrentApplication.PREFERENCE_STORAGE);

            switch (requestCode) {
                case RESULT_STORAGE:
                    s.onActivityResult(resultCode, data);
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
