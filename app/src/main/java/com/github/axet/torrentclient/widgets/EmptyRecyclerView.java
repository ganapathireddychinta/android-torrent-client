package com.github.axet.torrentclient.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.github.axet.androidlibrary.widgets.HeaderRecyclerView;
import com.github.axet.androidlibrary.widgets.WrapperRecyclerAdapter;

public class EmptyRecyclerView extends HeaderRecyclerView {

    View e;
    AdapterDataObserver empty = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            if (e != null) {
                Adapter a = getAdapter();
                while (a instanceof WrapperRecyclerAdapter)
                    a = ((WrapperRecyclerAdapter) a).getWrappedAdapter();
                if (a.getItemCount() == 0) {
                    setVisibility(GONE);
                    e.setVisibility(VISIBLE);
                    return;
                }
                e.setVisibility(GONE);
            }
            setVisibility(VISIBLE);
        }
    };

    public EmptyRecyclerView(Context context) {
        super(context);
    }

    public EmptyRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setEmptyView(View e) {
        this.e = e;
    }

    @Override
    public void setAdapter(Adapter adapter) {
        Adapter a = getAdapter();
        if (a != null)
            a.unregisterAdapterDataObserver(empty);
        super.setAdapter(adapter);
        if (adapter != null)
            adapter.registerAdapterDataObserver(empty);
    }
}
