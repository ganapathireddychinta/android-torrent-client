package com.github.axet.torrentclient.navigators;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.animations.ExpandItemAnimator;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.HeaderRecyclerView;
import com.github.axet.androidlibrary.widgets.PopupShareActionProvider;
import com.github.axet.androidlibrary.widgets.SearchView;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.androidlibrary.widgets.UnreadCountDrawable;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.animations.TorrentAnimation;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.dialogs.TorrentDialogFragment;
import com.github.axet.torrentclient.services.TorrentContentProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libtorrent.Libtorrent;

public class Torrents extends RecyclerView.Adapter<Torrents.TorrentHolder> implements DialogInterface.OnDismissListener,
        MainActivity.TorrentFragmentInterface, UnreadCountDrawable.UnreadCount,
        MainActivity.NavigatorInterface {
    public static final String TAG = Torrents.class.getSimpleName();

    public static final String PLAY = Torrents.class.getCanonicalName() + "_PLAY";
    public static final String STOP = Torrents.class.getCanonicalName() + "_STOP";

    public enum Filter {
        FILTER_ALL,
        FILTER_DOWNLOADING,
        FILTER_SEEDING,
        FILTER_QUEUED
    }

    int selected = -1;

    public Filter navfilter = Filter.FILTER_ALL;

    ArrayList<Storage.Torrent> filter = null;
    Context context;
    MainActivity main;
    TorrentDialogFragment dialog;
    HeaderRecyclerView list;
    Map<Storage.Torrent, Boolean> unread = new HashMap<>();
    BroadcastReceiver receiver;
    Storage storage;
    ExpandItemAnimator animators;

    public static void play(Context context, long t) {
        Intent i = new Intent(PLAY);
        i.putExtra("torrent", t);
        context.sendBroadcast(i);
    }

    public static void stop(Context context, long t) {
        Intent i = new Intent(STOP);
        i.putExtra("torrent", t);
        context.sendBroadcast(i);
    }

    public static class TorrentHolder extends RecyclerView.ViewHolder {
        public View base;
        public View play;
        public TextView title;
        public TextView time;
        public View playerBase;
        public ProgressBar bar;
        public ImageView stateImage;
        public TextView tt;
        public ImageView expand;
        public ImageView rename;
        public ImageView open;
        public View openSpace;
        public View share;
        public View trash;

        public TorrentHolder(View itemView) {
            super(itemView);
            base = itemView.findViewById(R.id.recording_base); // GridView unable to handle GONE views, hide its base.
            play = itemView.findViewById(R.id.torrent_play);
            title = (TextView) itemView.findViewById(R.id.torrent_title);
            time = (TextView) itemView.findViewById(R.id.torrent_status);
            playerBase = itemView.findViewById(R.id.recording_player);
            bar = (ProgressBar) itemView.findViewById(R.id.torrent_process);
            stateImage = (ImageView) itemView.findViewById(R.id.torrent_state_image);
            tt = (TextView) itemView.findViewById(R.id.torrent_process_text);
            expand = (ImageView) itemView.findViewById(R.id.torrent_expand);
            rename = (ImageView) itemView.findViewById(R.id.recording_player_rename);
            open = (ImageView) itemView.findViewById(R.id.recording_player_open);
            openSpace = itemView.findViewById(R.id.recording_player_open_space);
            share = itemView.findViewById(R.id.recording_player_share);
            trash = itemView.findViewById(R.id.recording_player_trash);
        }
    }

    public Torrents(MainActivity main, HeaderRecyclerView list) {
        super();

        this.main = main;
        this.context = main;
        this.list = list;
        this.storage = TorrentApplication.from(getContext()).storage;
        this.animators = new ExpandItemAnimator() {
            @Override
            public Animation apply(RecyclerView.ViewHolder h, boolean animate) {
                if (selected == h.getAdapterPosition())
                    return TorrentAnimation.apply(Torrents.this.list, h.itemView, true, animate);
                else
                    return TorrentAnimation.apply(Torrents.this.list, h.itemView, false, animate);
            }
        };

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a.equals(PLAY)) {
                    Storage.Torrent t = storage.find(intent.getLongExtra("torrent", -1));
                    play(t);
                }
                if (a.equals(STOP)) {
                    Storage.Torrent t = storage.find(intent.getLongExtra("torrent", -1));
                    stop(t);
                }
                if (a.equals(Storage.REMOTE_ACTION)) {
                    notifyDataSetChanged();
                }
            }
        };
        IntentFilter fi = new IntentFilter();
        fi.addAction(PLAY);
        fi.addAction(STOP);
        fi.addAction(Storage.REMOTE_ACTION);
        getContext().registerReceiver(receiver, fi);

        setHasStableIds(true);
    }

    public Context getContext() {
        return context;
    }

    @Override
    public void update() {
        if (dialog != null)
            dialog.update();
    }

    public void updateStorage() {
        for (int i = 0; i < storage.count(); i++) {
            Storage.Torrent t = storage.torrent(i);
            if (Libtorrent.torrentActive(t.t))
                t.update();
        }
        if (navfilter != Filter.FILTER_ALL)
            filter();
        notifyDataSetChanged();
    }

    public void close() {
        if (receiver != null) {
            getContext().unregisterReceiver(receiver);
            receiver = null;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (this.dialog != null)
            this.dialog.close();
        this.dialog = null;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (filter != null)
            return filter.size();
        return storage.count();
    }

    public Storage.Torrent getItem(int i) {
        if (filter != null)
            return filter.get(i);
        return storage.torrent(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).t;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public TorrentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View convertView = inflater.inflate(R.layout.torrent, parent, false);
        return new TorrentHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final TorrentHolder h, int position) {
        final Storage.Torrent t = getItem(position);

        Boolean u = unread.get(t);
        if (u != null && u)
            h.itemView.setBackgroundColor(ThemeUtils.getThemeColor(getContext(), R.attr.unreadColor));
        else
            h.itemView.setBackgroundColor(0);

        h.title.setText(t.name());
        h.time.setText(t.status());

        // cover area, prevent click over to convertView
        h.playerBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        // we need runnable because we have View references
        final Runnable delete = new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.delete_torrent);

                String name = Libtorrent.metaTorrent(t.t) ? ".../" + t.name() : t.name();

                builder.setMessage(name + "\n\n" + context.getString(R.string.are_you_sure));
                final Runnable stop = new Runnable() {
                    @Override
                    public void run() {
                        if (Torrents.this.dialog != null) { // prevent showing deleted torrent
                            Torrents.this.dialog.dismissAllowingStateLoss();
                            Torrents.this.dialog.close();
                            Torrents.this.dialog = null;
                        }
                        t.stop();
                    }
                };
                final Runnable remove = new Runnable() {
                    @Override
                    public void run() {
                        storage.remove(t);
                        if (filter != null)
                            filter.remove(t);
                        selected = -1; // do not notify old selection
                        main.updateUnread();
                    }
                };
                if (Libtorrent.metaTorrent(t.t)) {
                    builder.setNeutralButton(R.string.delete_with_data, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            stop.run();
                            Uri f = Storage.child(context, t.path, t.name());
                            Storage.delete(context, f);
                            remove.run();
                            notifyItemRemoved(h.getAdapterPosition());

                        }
                    });
                }
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        stop.run();
                        remove.run();
                        notifyItemRemoved(h.getAdapterPosition());
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        };

        h.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play(t);
            }
        });

        {
            long p = t.getProgress();

            int color = 0;
            String text = "";

            Drawable d = null;
            switch (Libtorrent.torrentStatus(t.t)) {
                case Libtorrent.StatusChecking:
                    d = ContextCompat.getDrawable(getContext(), R.drawable.ic_pause_24dp);
                    color = ThemeUtils.getThemeColor(getContext(), R.attr.player_selection);
                    text = p + "%";
                    break;
                case Libtorrent.StatusPaused:
                    d = ContextCompat.getDrawable(getContext(), R.drawable.ic_pause_24dp);
                    color = ThemeUtils.getThemeColor(getContext(), R.attr.colorButtonNormal);
                    text = p + "%";
                    break;
                case Libtorrent.StatusQueued:
                    d = ContextCompat.getDrawable(getContext(), R.drawable.ic_pause_24dp);
                    color = Color.GREEN;
                    text = "Qued";
                    break;
                case Libtorrent.StatusDownloading:
                    d = ContextCompat.getDrawable(getContext(), R.drawable.play);
                    color = ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent);
                    text = p + "%";
                    break;
                case Libtorrent.StatusSeeding:
                    d = ContextCompat.getDrawable(getContext(), R.drawable.play);
                    color = ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent);
                    text = "Seed";
                    break;
            }
            PorterDuffColorFilter filter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
            h.stateImage.setColorFilter(filter);

            if (t.fail()) {
                d = ContextCompat.getDrawable(getContext(), R.drawable.ic_exclamation);
                int color2 = ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent);
                PorterDuffColorFilter filter2 = new PorterDuffColorFilter(color2, PorterDuff.Mode.SRC_IN);
                h.stateImage.setColorFilter(filter2);
            }

            h.stateImage.setImageDrawable(d);

            h.bar.getBackground().setColorFilter(filter);
            h.bar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
            h.bar.setProgress((int) p);

            h.tt.setText(text);
        }

        if (selected == position) {
            h.rename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    main.renameDialog(t.t);
                }
            });
            if (!Libtorrent.metaTorrent(t.t)) {
                h.rename.setColorFilter(Color.GRAY);
                h.rename.setOnClickListener(null);
            } else {
                h.rename.setColorFilter(ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent));
            }

            final Intent intent = TorrentContentProvider.openFolderIntent(context, t);
            if (!TorrentContentProvider.isFolderCallable(context, intent)) {
                h.open.setVisibility(View.GONE);
                h.openSpace.setVisibility(View.GONE);
            } else {
                h.open.setVisibility(View.VISIBLE);
                h.openSpace.setVisibility(View.VISIBLE);
            }
            h.open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    main.startActivity(intent);
                }
            });

            KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (myKM.inKeyguardRestrictedInputMode()) {
                h.open.setColorFilter(Color.GRAY);
                h.open.setOnClickListener(null);
            } else {
                h.open.setColorFilter(ThemeUtils.getThemeColor(getContext(), R.attr.colorAccent));
            }

            h.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupShareActionProvider.show(context, h.share, PopupShareActionProvider.intent(Libtorrent.torrentName(t.t), Libtorrent.torrentMagnet(t.t)));
                }
            });

            h.trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delete.run();
                }
            });

            h.expand.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_expand_less_black_24dp));
            h.expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select(-1);
                }
            });
        } else {
            h.expand.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_expand_more_black_24dp));
            h.expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select(h.getAdapterPosition());
                }
            });
        }

        animators.onBindViewHolder(h, position);

        h.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog == null) { // prevent double dialogs
                    if (t.t == -1) {
                        Log.d(TAG, "show deleted torrent");
                        return;
                    }
                    showDetails(t.t);
                }
            }
        });

        h.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_context, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_delete) {
                            delete.run();
                            return true;
                        }
                        if (item.getItemId() == R.id.action_rename) {
                            main.renameDialog(t.t);
                            return true;
                        }
                        if (item.getItemId() == R.id.action_check) {
                            storage.checkTorrent(t.t);
                            notifyDataSetChanged();
                            return true;
                        }
                        return false;
                    }
                });
                popup.show();
                return true;
            }
        });
    }

    public void select(int pos) {
        if (selected != pos && selected != -1)
            notifyItemChanged(selected);
        selected = pos;
        if (pos != -1)
            notifyItemChanged(pos);
    }

    @Override
    public void install(final HeaderRecyclerView list) {
        unread.clear();

        list.setAdapter(this);
        list.setItemAnimator(animators);
        list.addOnScrollListener(animators.onScrollListener);

        for (int i = 0; i < storage.count(); i++) {
            Storage.Torrent t = storage.torrent(i);
            unread.put(t, t.message);
        }

        storage.clearUnreadCount();

        main.invalidateOptionsMenu();
    }

    @Override
    public void remove(HeaderRecyclerView list) {
        list.setItemAnimator(null);
        list.removeOnScrollListener(animators.onScrollListener);
    }

    public void showDetails(Long f) {
        TorrentDialogFragment d = TorrentDialogFragment.create(f);
        if (dialog != null)
            dialog.close();
        dialog = d;
        d.show(main.getSupportFragmentManager(), "");
    }

    @Override
    public int getUnreadCount() {
        return storage.getUnreadCount();
    }

    public void play(Storage.Torrent t) {
        int s = Libtorrent.torrentStatus(t.t);

        if (s == Libtorrent.StatusChecking) {
            Libtorrent.stopTorrent(t.t);
            notifyDataSetChanged();
            return;
        }

        if (s == Libtorrent.StatusQueued) {
            // are we on wifi pause mode?
            if (Libtorrent.paused()) // drop torrent from queue
                storage.stop(t);
            else { // nope, we are on library pause, start torrent
                start(t);
            }
            notifyDataSetChanged();
            return;
        }

        if (s == Libtorrent.StatusPaused)
            start(t);
        else
            storage.stop(t);

        notifyDataSetChanged();
    }

    public void stop(Storage.Torrent t) {
        storage.stop(t);
        notifyDataSetChanged();
    }

    void start(Storage.Torrent t) {
        String s = t.path.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE)) {
            File p = Storage.getFile(t.path);
            if (!p.exists() && !p.mkdirs()) {
                Toast.makeText(main, R.string.torrent_notfound, Toast.LENGTH_LONG).show();
                t.ejected = true;
                return;
            }
        }

        if (Storage.ejected(context, t.path)) {
            Toast.makeText(main, R.string.torrent_notfound, Toast.LENGTH_LONG).show();
            t.ejected = true;
            return;
        }
        t.ejected = false;

        if (t.readonly) {
            if (t.readonly()) {
                ErrorDialog.Error(main, main.getString(R.string.filedialog_readonly) + " " + Storage.getDisplayName(context, t.path));
                return;
            }
            t.readonly = false;
        }

        if (t.check || t.altered()) {
            Toast.makeText(main, R.string.torrent_altered, Toast.LENGTH_LONG).show();
            t.check();
            return;
        }

        long free = Storage.getFree(context, t.path);
        long left = storage.getPendingOnDiskRemaining(t);

        if (free < left) {
            Toast.makeText(main, R.string.no_space_left, Toast.LENGTH_LONG).show();
            t.check();
            t.readonly = true;
            return;
        }

        storage.start(t);
    }

    public boolean filter(Storage.Torrent t) {
        int s = Libtorrent.torrentStatus(t.t);
        boolean full = Libtorrent.metaTorrent(t.t) && Libtorrent.torrentPendingBytesCompleted(t.t) == Libtorrent.torrentPendingBytesLength(t.t);
        switch (navfilter) {
            case FILTER_SEEDING:
                return full;
            case FILTER_DOWNLOADING:
                return !full;
            case FILTER_QUEUED:
                return s == Libtorrent.StatusQueued || s == Libtorrent.StatusPaused || s == Libtorrent.StatusChecking;
            default:
                return true;
        }
    }

    public void filter(Filter f) {
        navfilter = f;
        if (navfilter == Filter.FILTER_ALL) {
            if (filter == null)
                return; // ignore udpate
            filter = null;
        } else {
            filter = new ArrayList<>();
            for (int i = 0; i < storage.count(); i++) {
                Storage.Torrent t = storage.torrent(i);
                if (filter(t))
                    filter.add(t);
            }
        }
        notifyDataSetChanged();
    }

    public void filter() {
        filter(navfilter);
    }

    @Override
    public void search(String q) {
        filter = new ArrayList<>();
        for (int i = 0; i < storage.count(); i++) {
            Storage.Torrent t = storage.torrent(i);
            if (filter(t) && SearchView.filter(q, t.name()))
                filter.add(t);
            else if (filter(t) && SearchView.filter(q, t.hash))
                filter.add(t);
        }
        notifyDataSetChanged();
    }

    @Override
    public void searchClose() {
        filter = null;
        notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem login = menu.findItem(R.id.action_login);
        MenuItem home = menu.findItem(R.id.action_home);
        MenuItem grid = menu.findItem(R.id.action_grid);
        grid.setVisible(false);
        home.setVisible(false);
        login.setVisible(false);
        return true;
    }
}
